package grapics;
import java.math.*;
public class Triangle extends Shape {


    private int height;
    private int width;
    private int perpendicular;
    private static int count;
    private final int ANGELS=3;
    public final String DESCREPTION="Triangle";


    public Triangle() throws NegativeSizeException{
        this(0,0,0,0,0);
    }
    public Triangle(Triangle copy) throws NegativeSizeException{
        this(copy.height,copy.width,copy.perpendicular,copy.getX(),copy.getY());
    }
    public Triangle(int height, int width, int perpendicular) throws NegativeSizeException{
        this(height,width,perpendicular,0,0);
    }
    public Triangle(int height, int width, int perpendicular, int x, int y) throws NegativeSizeException{
        this.height=height;
        this.width=width;
        this.perpendicular=perpendicular;
        this.setX(x);
        this.setY(y);
        count++;

    }


//**********Getters and Setters

    public void setPerpendicular(int perpendicular) throws NegativeSizeException {
        if(perpendicular<0) throw new NegativeSizeException("Negative Perpendicular !!");
        else this.perpendicular = perpendicular;  }

    public void setHeight(int height)throws NegativeSizeException{
        if (height<0) throw new NegativeSizeException("Negative Height !!");
        else this.height=height;}

    public void setWidth(int width)  throws NegativeSizeException{
        if(width<0) throw new NegativeSizeException("Negative width !!");
        else this.width=width; }

    public int getWidth() {  return width;  }

    public int getHeight() {   return height;  }

    public int getPerpendicular() {   return perpendicular;  }

    public static int getCount() {  return count;   }

    public int getANGELS() { return ANGELS;   }

    public double getArea(){ return (double)((getHeight()*getWidth())/2);
    }
    public double getPerimeter(){
        double side1;
        double side2;
        double side3;
        side1=(Math.sqrt((height*height)+(perpendicular*perpendicular)));
        side2=(Math.sqrt((height*height)+((width-perpendicular)*(width-perpendicular))));
        side3= width;
        return side1+side2+side3;
    }
    public String getDESCREPTION(){
        return  DESCREPTION;
    }

    @Override
    public void draw(DrawingContext dc) {
        dc.draw(this);
    }

    @Override
    public void scale(int s) {
        height = (height*s)/100;
        width =(width*s)/100;
        perpendicular=(perpendicular*s)/100;
    }
    public String toString() {
        return "Triangle: \n" +
                "X position = "+ getX()+
                "\nY position = "+ getY()+
                "\nHeight = " + height +
                "\nWidth = " + width +
                "\nPerpendicular= "+getPerpendicular()+
                "\nDESCREPTION= " + DESCREPTION+"\n***************";
    }
}

