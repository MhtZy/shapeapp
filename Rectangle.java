package grapics;

public class Rectangle extends Shape {


    public double height;
    public double width;
    private static int count=0;
    private static final int Angels=4;
    public final String DESCREPTION="Rectangle";



/// Constructors and overloading
    public Rectangle(){
        this(0,0,0,0);

    }
    public Rectangle(Rectangle rect){
        this(rect.getX(),rect.getY(),rect.height,rect.width);
    }
    public Rectangle(int x, int y){
        this(x,y,0,0);

    }
    public Rectangle(double height, double width){
        this(0,0,height,width);
    }

    public Rectangle(int x, int y, double height, double width){
        setX(x);
        setY(y);
        this.height =height<0?-height:height;
        this.width=width<0?-width:width;
        count++;
    }

    ///**************** Printing Information of Rectangle
    public void infoPrints(){


        System.out.println("Your value of X: "+getX());
        System.out.println("Your value of Y: "+getY());
        System.out.println("Your value of Height: " + this.width+" cm");
        System.out.println("Your value of Width: " + this.height+" cm");
        System.out.println("Your perimeter value of "+this.getDESCREPTION()+": "+getPerimeter()+" centimeter");
        System.out.println("your area value of "+this.getDESCREPTION()+": "+getArea()+" meter square");
        System.out.format("%s heeft %d hoeken\n",this.getDESCREPTION(),Rectangle.Angels);
        System.out.format("The type of object is: %s\n",this.getDESCREPTION());
        System.out.format("Rectangle count is: %d \nSquare count is: %d\n",(Rectangle.getCount()-Square.getCountSquare()),Square.getCountSquare());

    }
    public static int getCount(){
        return count;
    }
    public static final int ANGELS(){
        return Angels;
    }
    public String getDESCREPTION(){
        return  DESCREPTION;
    }



    /** Getting Perimeter*/
    public double getPerimeter(){return ((2*height)+(2*width));}

    /**Getting Area Values*/
    public double getArea(){return (height*width);}


    /**Grow Method*/
    public void grow(int d){this.height=getHeight()+d; this.width= getWidth()+d; }

    /**Setting Whole Position of Rectangel*/
    public void setPosition(int x, int y){
        setX(x);
        setY(y);  }

    /**Setting only X value*/


    /**Setting Height value*/
    public void setHeight(double height) throws NegativeSizeException{
        if (height<0) throw new NegativeSizeException("Negative Height !!");
        else this.height=height;
    }

    /**Setting Width value*/
    public void setWidth(double width) throws NegativeSizeException{
        if(width<0) throw new NegativeSizeException("Negative width !!");
        else this.width=width; }

    ///**************** Getting Height value
    public double getHeight() {  return height; }

    ///**************** Getting Height value
    public double getWidth() {  return width; }


    @Override
    public void draw(DrawingContext dc) {
        dc.draw(this);
    }

    @Override
    public void scale(int s) {
        height = (height * s)/100;
        width = (width * s)/100;

    }

    @Override
    public String toString() {
        return "Rectangle: \n" +
                "X position= "+ getX()+
                "\nY position= "+ getY()+
                "\nHeight= " + height +
                "\nWidth= " + width +
                "\nDESCREPTION= " + DESCREPTION+"\n***************";
    }
}
