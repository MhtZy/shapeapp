package grapics;

public class Square extends Rectangle {

public final String DESCREPTION= "Square";
private static int countSquare=0;




    public Square(){
        this(0);
    }
    public Square(int side){
        this(0,0,side);
    }
    public Square(Square copy){
        this(copy.getX(),copy.getY(),(int)copy.width);
    }
    public Square(int x, int y,int side){
        super((double)side,(double)side);
        setSide(side);
        countSquare++;


    }

    public static int getCountSquare(){
        return countSquare;
    }
    public int getSide(){
        return (int)getHeight();
    }
    public void setSide(int side){
        super.setHeight(side);
        super.setWidth(side);

    }
        public void setHeight(int height){
        setSide(height); }

        public void setWidth(int width){
        setSide(width); }

        public String getDESCREPTION(){
            return  DESCREPTION;
        }




}
