package grapics;

import java.util.Random;

public class ShapeApp {
    public static void main(String[] args) {
        final int MAX_SHAPES = 50;
        Random rand = new Random();
        String[] classes = {Rectangle.class.getName(), Circle.class.getName(), Square.class.getName(), Triangle.class.getName(), IsoScalesTriangle.class.getName()};
        String[] generatedClasses = new String[MAX_SHAPES];
        for( int i =0; i< generatedClasses.length; i++ ){
            generatedClasses[i] = classes[rand.nextInt(classes.length)];
        }
        Shape[] shapes = new Shape[MAX_SHAPES];

        //System.out.println(classes[1]);
        int count = 0;
        for (String s : generatedClasses) {
           // System.out.println(s);
            addToList(s, count++, shapes);
        }
        for (Shape shape : shapes) {
            if (shape != null)
                printShape(shape);
        }

    }

    private static void addToList(String className, int index, Shape[] list){
        Random rnd = new Random();
        switch (className){

            case "grapics.Rectangle":
                list[index] = new Rectangle(rnd.nextInt(100),rnd.nextInt(50),rnd.nextDouble()*10,rnd.nextDouble()*10);
                break;
            case "grapics.Circle":
                list[index] = new Circle(rnd.nextInt(100),rnd.nextInt(50),rnd.nextInt(50));
                break;
            case "grapics.Triangle":
                list[index] = new Triangle(rnd.nextInt(100),rnd.nextInt(50),rnd.nextInt(25),rnd.nextInt(25),rnd.nextInt(25));
                break;
            case "grapics.Square":
                list[index] = new Square(rnd.nextInt(100),rnd.nextInt(100),rnd.nextInt(25));
            break;
            case "grapics.IsoScalesTriangle":
                list[index] = new IsoScalesTriangle(rnd.nextInt(100),rnd.nextInt(100),rnd.nextInt(25));
                break;
            default:
                System.out.println("Unknown type");
        }


    }

    private static void printShape(Shape s) {
        System.out.print("Class " + s.getDESCREPTION() + "\n");
        System.out.print("Area " + s.getArea() + "\n");
        System.out.print("Perimeter " + s.getPerimeter() + "\n");
        System.out.print("Position (" + s.getX() + "," + s.getY() + ")\n");

    }

}
