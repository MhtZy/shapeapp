package grapics;
import java.util.Iterator;
public class DrawableIterator implements Iterator<Drawable>  {
    Drawing d;
    int index = 0;

    public DrawableIterator(Drawing d){ this.d=d; }
    @Override
    public boolean hasNext() {  return index<d.getSize();   }


    @Override
    public Drawable next() {      return d.getDrawables(index++);    }
}
