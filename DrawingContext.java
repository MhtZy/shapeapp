package grapics;

public interface DrawingContext {


    public abstract void draw(Rectangle rectangle);
    public abstract void draw(Circle circle);
    public abstract void draw(Triangle triangle);

}
