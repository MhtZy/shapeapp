package grapics;

public class IsoScalesTriangle extends Triangle {
    public final String DESCREPTION="Iso Scale Triangle";
    private static int countIso;

    public IsoScalesTriangle(){
        this(0,0,0,0,0);
    }
    public IsoScalesTriangle(Triangle copy){
        this(copy.getHeight(),copy.getWidth(),copy.getPerpendicular(),copy.getX(),copy.getY());

    }
    public IsoScalesTriangle(int h, int w, int p){
        this(h,w,p,0,0);
    }
    public IsoScalesTriangle(int h, int w, int p, int x, int y){
        super(h,w,p);
        setX(x);
        setY(y);
        countIso++;
    }
    public void setWidth(int w){
        super.setWidth(w);
        super.setPerpendicular(w/2);
    }
    public void setPerpendicular(int p){ setWidth(2*p); }
    public int getCountIso(){return countIso;}
    public String getDESCREPTION(){
        return  DESCREPTION;
    }


}
