package grapics;
import java.math.*;



public class CalcCos{

    static final double E =2.718281828459045;
    static final double PI = 3.141592653589793;



    public static void main(String[] args) {

                for (double i=0;i<=2*PI;i+=0.1f){

                    System.out.format("%.2f rad : %.2f deg  : cos = %.2f \n",i,Math.toDegrees(i), Math.cos(i));
                }
    }

}
