package grapics;

public abstract class Shape implements Drawable{
    private int x;
    private int y;
    private static int count;
    public final String DESCREPTION="Shape";

    public Shape(){
        this(0,0);
    }
    public Shape(int x, int y){
        this.x=x;
        this.y=y;
        count++;
    }
    public void setX(int x){ this.x=x;}

    public void setY(int y){this.y=y;}

    public void setPosition(int x, int y){ this.x=x; this.y=y;}

    public int getX(){return this.x;}
    public int getY(){return this.y;}
    public static int getCount(){return count;}

    public abstract double getArea();
    public abstract double getPerimeter();
    public String getDESCREPTION(){return DESCREPTION;}



}
