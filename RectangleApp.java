package grapics;
import static grapics.Rectangle.ANGELS;
import static grapics.Rectangle.getCount;
public class RectangleApp {
    public static void main(String[] args) {
        System.out.println("This program used for a rectangle measurement");
        Rectangle rect = new Rectangle(-10,-20,-10,-20);
        Rectangle rect2= new Rectangle(2,5);
        Rectangle rect3= new Rectangle(rect2);
        rect.infoPrints();
        rect2.infoPrints();
        rect3.infoPrints();
        System.out.println("Total Rectangle used: "+ANGELS());
        System.out.println("Total hoeken van Rechthoeken: "+(getCount()*ANGELS()));


    }
}
