package grapics;

public interface Scaleable {
    public static final int QUARTER= 25;
    public static final int HALF = 50;
    public static final int DOUBLE = 200;

    public abstract void scale(int s);

    public default void ScaleDouble(){
        scale(DOUBLE);
    }

    public default void ScaleHalf(){
        scale(HALF);
    }
    public default void ScaleQuarter(){
        scale(QUARTER);
    }

}

