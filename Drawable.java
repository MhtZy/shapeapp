package grapics;

public interface Drawable extends Scaleable {

    public abstract void draw( DrawingContext dc);

}
