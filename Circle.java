package grapics;

public class Circle extends Shape{

    private int radius;
    private static int count=0;
    public static int ANGELS=0;
    static final double PI = 3.141592653589793;
    public final String DESCREPTION="Circle";

    public Circle() throws NegativeSizeException{
        this(0,0,0);
    }
    public Circle(int radius) throws NegativeSizeException{
        this(0,0,radius);
    }
    public Circle(Circle c) throws  NegativeSizeException{
        this(c.getX(),c.getY(),c.radius);
    }
    public Circle(int x, int y, int radius) throws NegativeSizeException {

        setX(x);
        setY(y);
        this.radius= radius<0?-radius:radius;
        count++;
    }
    public void setPosition(int x, int y){
        setX(x);
        setY(y);
    }


    public int grow(int d){
       return this.radius+=d;
    }

    public int getRadius() {      return radius;   }

    public void setRadius(int radius) throws NegativeSizeException {
        if(radius<0) throw new NegativeSizeException("Negative Radius !!");
        else this.radius = radius;  }

    public static int getCount(){
        return count;
    }


    @Override
    public double getArea() {
        return (double)(PI*(getRadius()*getRadius()));

    }


    @Override
    public double getPerimeter() {
        return (2*PI*getRadius());
    }
    public String getDESCREPTION(){
        return  DESCREPTION;
    }

    public void infoPrints(){


    System.out.println("Your value of X: "+this.getX());
    System.out.println("Your value of Y: "+this.getY());
    System.out.println("Your value of radius: " + this.radius);
    System.out.format("Your perimeter value of Circle: %.2f \n",this.getPerimeter());
    System.out.format("your area value of Circle: %.2f\n",this.getArea());
    System.out.println("Circle heeft "+Circle.ANGELS);
    System.out.format("According to %.2f Degree, Cosinus is %.2f \n",Math.toDegrees(ANGELS), Math.cos(Math.toRadians(ANGELS)));

    }

    @Override
    public void draw(DrawingContext dc) {
        dc.draw(this);
    }

    @Override
    public void scale(int s) {
        radius = (radius*s)/100;

    }
    public String toString() {
        return "Circle: \n" +
                "X position = "+ getX()+
                "\nY position = "+ getY()+
                "\nRadius = " + getRadius()+
                "\nDESCREPTION= " + DESCREPTION+"\n***************";
    }
}
