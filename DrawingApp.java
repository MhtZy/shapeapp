package grapics;



public class DrawingApp {

    public static void main(String[] args) {
        Drawing dr = new Drawing();
        Drawable rect1= new Rectangle(10,10,15,10);
        Drawable circle = new Circle(5,5,15);
        Drawable triangel = new Triangle(4,5,3,7,7);
        TextDrawContext drc = new TextDrawContext();
        dr.add(rect1);
        dr.add(circle);
        dr.add(triangel);
        //dr.draw(drc);
        DrawableIterator drawableIterator = new DrawableIterator(dr);

        for (DrawableIterator it = drawableIterator; it.hasNext(); ) {
            Drawable ds = it.next();
            System.out.println(ds);
        }




    }
}
