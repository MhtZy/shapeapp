package grapics;

import java.util.Iterator;

public class Drawing implements Drawable, Iterable<Drawable> {
    public static int MAX = 4;
    private int listSizeMultiplier = 1;
    private Drawable[] drawables = new Drawable[MAX];
    private int size = 0;

    public void add(Drawable s) {
        if (s == null || size >= drawables.length || locate(s) != -1) {
            if (size >= drawables.length) {
                System.out.println("List full, resizing");
                Drawable[] tempDrawables = new Drawable[MAX*(++listSizeMultiplier)];
                int i = 0;
                for(Drawable d:drawables){
                    tempDrawables[i++] = d;
                }
                drawables = tempDrawables;
                System.out.println("List size: " + drawables.length);
            } else {
                System.out.println("Not adding drawables: " + s);
                if (s != null) {
                    System.out.println("The drawable is already in the list");
                }
                return;
            }
        }
        drawables[getFreeLocation()] = s;
        size++;
    }

    public boolean isPersent(Drawable s) {
        return s != null && locate(s) != -1;
    }

    private int locate(Drawable s) {
        for (int i = 0; i < drawables.length; i++) {
            if (drawables[i] == s) {
                return i;
            }
        }
        return -1;
    }

    public void remove(Drawable s) {
        int l = locate(s);
        if (l > 0) {
            drawables[l] = null;
            size--;
        } else {
            System.out.println("Drawable was never in drawing to begin with: " + s);
        }
    }

    private int getFreeLocation() {
        return locate(null);
    }

    public void clear() {
        for (Drawable d : drawables) d = null;
        size = 0;
    }

    public int getSize() {
        return size;
    }

    public void draw(DrawingContext dc){
        System.out.println("Drawing: ");
        for (Drawable d: drawables){
            if(d!=null){
                //System.out.println(d);
                //dc.draw();
                d.draw(dc);
            }
        }
    }

    @Override
    public void scale(int s) {

    }
    public Iterator<Drawable> iterator(){
        return null;
    }
    public Drawable getDrawables(int d){return drawables[d];   }



}
