package grapics;

public class Logo {
    private Rectangle r= new Rectangle();
    private Circle c= new Circle();
    private String str = new String("");
    public Rectangle[] rArray= new Rectangle[100];
    private Circle[] cArray= new Circle[100];
    private String[] strArray = new String[100];
    private static int countLogo;
    private static int countRectArray;
    private static int countCirArray;
    private static int countStrArray;


    public Logo(){
         this(null,null,null);
    }
    public Logo(Rectangle r, Circle c, String text){
         this.r=r;
         this.c=c;
         this.str = str;
         countLogo++;

     }
     public Rectangle getR(){return r;}
     public Circle getC(){return c;}
     public String getStr(){return str;}

     public Rectangle setR(Rectangle r){
        return this.r=r;
     }
     public Circle setC(Circle c){
        return this.c=c;
     }
     public String setT(String str){
        return this.str =str;
     }

     public double getArea(){
        double rArea =r.getArea();
        double cAreaCircle =c.getArea();
        return rArea+cAreaCircle;
     }

    public static int getCountRectArray() { return countRectArray; }

    public static int getCountCirArray() { return countCirArray; }

    public static int getCountStrArray() { return countStrArray; }

    public static int getCountLogo() { return countLogo; }


    public void addArray(Rectangle r, Circle c, String str ){

        rArray[countRectArray]= r;
        cArray[countCirArray]=c;
        strArray[countStrArray]=str;
        countRectArray++;
        countCirArray++;
        countStrArray++;



     }
}
